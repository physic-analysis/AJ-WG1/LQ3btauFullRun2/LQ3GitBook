# AnalysisStrategy
## Leptoquark theory
### Model parameter
Collider experiments provide the direct cross section limit.
The pair production can be occurred via these following interactions.

- lambda : The leptoquark Yukawa coupling152
- beta

### Signal simulation
Signal samples were generated at next-to-leading order in QCD with MadGraph5_aMC@NL0 2.4.3.

Madspin was used for the decay of the LQ. The parameter lambda was set to 0.3, resulting in a LQ width of about 0.2% of its mass.
The samples were produced for a model parameter of beta=0.5.
The branching ratio B and the model parameter beta is not equivalent, thus
