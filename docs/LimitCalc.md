# Limit calculation
## WSMaker_HH_bbtautau
- https://gitlab.cern.ch/physic-analysis/AJ-WG1/WSMaker_HH_bbtautau

You can calculate the cross section limit using WSMaker_HH_bbtautau.
This package is the forked ones from the original WSMaker_HH_bbtautau packages.
```
source setup.sh HHbbtautau
```
Firstly, you should compile this package.
```
cd build ; cmake ..; make
```
Put the configuration file.
SplitInputs
run_split.sh
SplitInputs -v LQHH190413 -r Run2 -s BDTScorePreselection -inDir /eos/atlas/unpledged/group-tokyo/users/ktakeda/LimitSetting_New_ktakeda/WSMaker/InputFiles/
This executable file is to split input files accorting to a configuration file under the inputConfigs/XXX.txt
```
run_Analysis_HH.sh
scripts/HHbbtautau/Analysis_HH.py
limit_lq.py
```
Put the configuration file.
Output the limit plot. You can see these files at the top of the directory.
```
python WSMaker_HH_bbtautau/scripts/analLQLepHad.py
```
This is the wrapper class for the WSMakerCore. The scripts calls the WSMakerCore/src/engin.cpp. The engin.cpp calls hist2workspace (This is the HistFactory execution code.)

## Statistical theory
### Introduction
This WSMaker_HH_bbtautau package is the higher level classes, which works as the wrpper scripts. The important one is WSMakerCore.

- src/engin.cpp
  - This calls HistFactory, namely hist2work, to calculate the log-likelihood from the input BDT scores.
- runAsymptoticCLs.C
  - This is the main logic to calculate the limit. As the script name shows, we use the asymptotic CLs method.

### Asymptotic CLs
### Asimov data
