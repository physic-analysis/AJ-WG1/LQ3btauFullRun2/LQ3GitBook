# MIA
MIA is one of the frameworks developed for HH->bbtautau analysis. MIA setup instruction is shown here. How to run MIA is described in MIA/README.md . To change running setup e.g. input datasets, modify MIA/config/hhbbtautau_hadhad.config . MIA basically intends to run with specific sample, so -p options is almost mandatory. e.g.
```
FinalHHbbtautauHH -p 2
```
After running each sample, need to merge them with "rhadd" command. And then plotting can be done with
```
python MIA/python/plot2TauHadHad.py
```
input files are indicated in MIA/python/steer2TauHadHad.py

## Each file discriptions
The detailed information can be show in the DoxyGen file. The simple descriptions will be discussed here.
- MIASetup/bootstrap/packages.txt : To specify each package version
- MIA/Root/MIARecSampleReader.cxx : You can see the sample numbers

## Useful Twiki links
- https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/XtoYYtobbtautau
- https://twiki.cern.ch/twiki/bin/view/AtlasProtected/CxAODFramework
- https://twiki.cern.ch/twiki/bin/view/AtlasProtected/LQ3LQ3btaubtau2016


## How to clone the MIA environment
To use MIA environment, at first you should download each package via GitLab. You can use the MIASetup package to construct the environment.
MIASetup README : https://gitlab.cern.ch/MIA/MIASetup/blob/master/README.md
This MIASetup/bootstrap/setup.sh script will help you to get all of the needed packages. The setup.sh scropt reads MIASetup/bootstrap/package.txt file which specify the package version. Thus if it is needed to use a specified version, you should edit the text file. Basically, the latest master branch will be cloned to your directory.

## How to setup
cd build/
setupATLAS; asetup --restore; source x*/setup.sh
MIA work flow
Create TauLH files using FinalHHbbtautauLH command with the correct options. The options can be given as the command line arguments. Please see the script (https://gitlab.cern.ch/physic-analysis/AJ-WG1/LeptoQuarkAnalysis/blob/master/FinalHHbbtautau/SR_LqSignalSample.sh), which can be used to run on the Lepto-quark signal MC samples.
Merge these output files.
Move to your python_plot directory, and do
python MIA/python/plot2Tau.py
. You can get a output directory including the output files.
You can use the LeptoquarkAnalysis package to do the LQ(lep-had) analysis work flow. [ https://gitlab.cern.ch/physic-analysis/AJ-WG1/LeptoQuarkAnalysis ]

## To create new mass limit
Tool name       Output name     Description
FinalPlotsCreator       testNtup        This file is the BDT input files.
BDTTraining     weight xml files        To create new BDT algorithm.
FinalPlotsCreator       FinalPlots      With the new BDT weight, you can create the BDT score distribution.
WSMaker Limit png       Create new Limit setting files.

## To create data-driven fake factor
Tool name       Output name     Description
DataDrivenFakeFactor    FinalPlots.Fake  
MIA/python/FF.py        a ROOT file containing fake factor       
FinalPlotsCreator       FinalPlots      With new data-driven fake factor
How to add new variables in a FinalPlots file (lephad)
If you want to add new variables to finalPlots root file, you should edit the MIAPlots::FillHistosMVAOutput() function, which calls the HistSvc::BookFillHist() function to register the user defined histograms. Each FillHistos*() functions can define the corresponding TDirectory, e.g. If you call the FillHistosMVAOutput() the MIA program will output MVA variables under the BDTVarsPreselection TDirectory.
MIAPlots::FillHistosMVAOutput : Fill MVA variables for lep-had channel
Cut flow
MIA2Tau::Execute( )
TauEtaCut
TauPtCut
ZTauCut
JetPtCut
TruthTaggingDoubleCountingCut
ZMassLow
Options
FinalHHbbtautau command needs some options, but the option name may not make sense to you. This is because I'll summarize the descriptions.
Variable name   Option name     Description
DoFakeTau       fake    runs anti-tau region
FakeFactor      ff      calculates Fake Factor. If this option is true, the output file will have FF_All TDirectory.
outputNtupName  outputNtupName  For the MVA study, If the option is true, you can create the Ntuples for the BDT training.
Each file discriptions
MIA
File name       Function name   Description
WeightSvc.cxx   TauFakeFactor() Return the fake-factor
